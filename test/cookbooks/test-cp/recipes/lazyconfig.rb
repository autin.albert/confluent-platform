# frozen_string_literal: true

#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inject some configuration keys
{
  'kafka' => '/etc/kafka/server.properties',
  'registry' => '/etc/schema-registry/schema-registry.properties',
  'rest' => '/etc/kafka-rest/kafka-rest.properties'
}.each_pair do |component, file|
  node[cookbook_name][component].each_pair do |key, value|
    node.run_state['confluent-platform'] ||= {}
    node.run_state['confluent-platform'][component] ||= {}
    node.run_state['confluent-platform'][component][file] ||= {}
    node.run_state['confluent-platform'][component][file][key] = value
  end
end
