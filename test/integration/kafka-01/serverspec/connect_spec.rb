# frozen_string_literal: true

#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Kafka Connect Daemon' do
  it 'is running' do
    expect(service('kafka-connect')).to be_running
  end

  it 'is launched at boot' do
    expect(service('kafka-connect')).to be_enabled
  end

  waiting('kafka-connect', 8083)

  it 'is listening on port 8083' do
    expect(port(8083)).to be_listening
  end
end

describe 'Kafka Connect Configuration' do
  servers = (1..2).map { |i| "PLAINTEXT://kafka-kitchen-0#{i}.kitchen:9092" }
  describe file('/etc/kafka/connect-distributed.properties') do
    its(:content) { should eq <<-PROP.gsub(/^ {4}/, '') }
    # Produced by Chef -- changes will be overwritten

    bootstrap.servers=#{servers.join(',')}
    config.storage.replication.factor=2
    config.storage.topic=connect-configs
    group.id=connect-cluster
    key.converter=org.apache.kafka.connect.json.JsonConverter
    key.converter.schemas.enable=true
    offset.flush.interval.ms=10000
    offset.storage.partitions=25
    offset.storage.replication.factor=2
    offset.storage.topic=connect-offsets
    plugin.path=/usr/share/java,/usr/share/confluent-hub-components
    status.storage.partitions=5
    status.storage.replication.factor=2
    status.storage.topic=connect-status
    value.converter=org.apache.kafka.connect.json.JsonConverter
    value.converter.schemas.enable=true
    PROP
  end

  describe file('/etc/kafka/connect-log4j.properties') do
    its(:content) { should contain 'log4j.rootLogger=INFO, stdout' }
    its(:content) { should contain '# Kitchen=true' }
  end
end

describe 'Confluent Hub' do
  it 'should have installed Elastic Search connector' do
    filename = '/usr/share/confluent-hub-components/'\
      'confluentinc-kafka-connect-elasticsearch/manifest.json'
    expect(file(filename)).to exist
  end
end

describe 'Connector recipe & resource' do
  it 'to-delete-file-source should not exist' do
    url = 'http://localhost:8083/connectors/to-delete-file-source'
    cmd = "http_proxy='' curl -s #{url} 2>&1"
    expect = '{"error_code":404,"message":'\
      '"Connector to-delete-file-source not found"}'
    expect(command(cmd).stdout).to eq expect
  end

  it 'local-file-source should be running' do
    url = 'http://localhost:8083/connectors/local-file-source/status'
    cmd = "http_proxy='' curl -s #{url} 2>&1"
    expect(command(cmd).stdout).to contain '"state":"RUNNING"'
  end

  it 'local-file-sink should be running' do
    url = 'http://localhost:8083/connectors/local-file-sink/status'
    cmd = "http_proxy='' curl -s #{url} 2>&1"
    expect(command(cmd).stdout).to contain '"state":"RUNNING"'
  end

  it 'file sink should exist' do
    expect(file('/tmp/sink-test.txt')).to exist
  end
end
